
public class Program {
	
	static void myfun(int num) {//Widening
		System.out.println("Inside int");
	}
	
	static void myfun(Integer num) {//Boxing
		System.out.println("Inside Integer");
	}
	
public static void main(String[] args) {
	
	Integer num1 = 10; // boxing
	int num2=num1; //unBoxing
	
	short num3=20;
	myfun(num3);
	
	System.out.println(num2);
	
}	

}
